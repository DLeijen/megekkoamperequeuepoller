import QueuePoller
import MailService
import MailDetails
import OrderDetails

from time import sleep
from getpass import getpass
from datetime import datetime


def main():
    password = getpass(prompt='Enter password for {}:'.format(MailDetails.sender_mail))
    current_position = {}

    while True:
        for order_id in OrderDetails.order_ids:
            count_failed_requests = 0
            new_queue_status = QueuePoller.poll_queue(order_id=order_id)

            while new_queue_status[0] == -1 and count_failed_requests < 10:
                sleep(15)
                new_queue_status = QueuePoller.poll_queue(order_id=order_id)
                count_failed_requests += 1

            if new_queue_status[0] == -1:
                print('{}: Failed to retrieve position for order {}'.format(datetime.now(), order_id))
            elif order_id not in current_position or new_queue_status[0] != current_position.get(order_id):
                print('{}: New queue position {} for order {}'.format(datetime.now(), new_queue_status[0], order_id))
                MailService.send_position_update(order_id,
                                                 password,
                                                 current_position.get(order_id),
                                                 new_queue_status[0],
                                                 new_queue_status[1])
                current_position[order_id] = new_queue_status[0]
            else:
                print('{}: Still in position {} for order {}'.format(datetime.now(),
                                                                     current_position.get(order_id),
                                                                     order_id))
            sleep(10)
        sleep(60 * 60)


if __name__ == "__main__":
    main()
