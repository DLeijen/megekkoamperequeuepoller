import OrderDetails as od

import requests
from bs4 import BeautifulSoup
import re
import random


def poll_queue(order_id):
    regex_position = re.compile(r"(\d+) van de \d+")
    user_agent_list = [
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.1.1 Safari/605.1.15',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:77.0) Gecko/20100101 Firefox/77.0',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36',
    ]

    # Pick a random user agent
    user_agent = random.choice(user_agent_list)
    # Set the headers
    headers = {
        'User-Agent': user_agent,
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        'Accept-Language': 'en-US,en;q=0.5',
        'DNT': '1',
        'Connection': 'keep-alive',
        'Upgrade-Insecure-Requests': '1',
        'origin': 'https://www.megekko.nl',
        'referer': 'https://www.megekko.nl/info/RTX-3080',
    }

    # Making a POST request
    r = requests.post('https://www.megekko.nl/scripts/wachtrij/wachtrij.php',
                      data={'ajax': 'lookup',
                            'orderid': order_id,
                            'postcode': od.postal_code},
                      headers=headers)

    # Read the content of the response
    json = r.json()

    # Check that request was succesful
    if str(json["status"]).lower() != "ok":
        return -1, None

    # print content of request
    response = json["data"]
    formatted_response = str(BeautifulSoup(response, features="html.parser").prettify())

    queue_pos_match = regex_position.search(formatted_response)

    queue_position = int(queue_pos_match.group(1)) if queue_pos_match is not None else -1

    return queue_position, formatted_response
