import MailDetails as md

import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def send_position_update(order, password, old_position, new_position, html):
    message = MIMEMultipart("alternative")
    message["Subject"] = 'New position in queue for order {}: {}'.format(order, new_position)
    message["From"] = md.sender_mail
    message["To"] = md.receiver_mail

    # Create the plain-text and HTML version of your message
    text = ''
    if old_position is not None:
        text = 'Your position changed from {} to {} for order {}.'.format(old_position, new_position, order)
    else:
        text = 'Your position in the queue is {} for order {}.'.format(new_position, order)

    # Wrap the text in html tags to prepend to the html block
    html = '<p>{}</p><hr><p>{}</p>'.format(text, html)

    # Turn these into plain/html MIMEText objects
    part1 = MIMEText(text, "plain")
    part2 = MIMEText(html, "html")

    # Add HTML/plain-text parts to MIMEMultipart message
    # The email client will try to render the last part first
    message.attach(part1)
    message.attach(part2)

    # Create secure connection with server and send email
    with smtplib.SMTP_SSL(md.smtp_server, md.smtp_port) as server:
        server.login(md.sender_mail, password)
        server.sendmail(
            md.sender_mail, md.receiver_mail, message.as_string()
        )